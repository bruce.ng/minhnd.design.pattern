package behavioural.strategy;

public interface Logging {
    void write(String message);
}
